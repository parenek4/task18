const members =
 [
    {
      id:1,
      name: 'John Doe',
      email: 'john@gmail.com',
      status: 'active'
    },
    {id: 2,
      name: 'Robin Eriksson',
      email: 'robin@gmail.com',
      status: 'inactive'
    },
    {
      id: 3,
      name: 'Max Steven',
      email: 'max@gmail.com',
      status: 'active'
    }
  ];
  

  module.exports = members;