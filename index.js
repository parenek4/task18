const express = require('express');
const path = require('path')

const app = express();
// const logger = (req, res, next) => {
//   console.log(`${req.protocol} ://${req.get('host')}${req.originalUrl}`);
//   next();
// }


// init middleware
  // app.use(logger);

  // Body Parser Middleware
  app.use( express.json());
  app.use(express.urlencoded({extended: false}));
  
  

// Setting a stat folder .use is method to include middleware
app.use(express.static(path.join(__dirname, 'html_pages')));

// Members API Routs

app.use('/api/members', require('./routes/api/members'));

// Linking pages

app.get('/contact', (req, res)=>{
  res.sendFile(path.join(__dirname, 'html_pages', 'contact.html' ))
});
app.get('/about', (req, res)=>{
  res.sendFile(path.join(__dirname, 'html_pages', 'about.html' ))
});

app.get ('/home', (req, res)=>{
  res.sendFile(path.join(__dirname, 'html_pages', 'home.html' ))
});

app.get ('/index', (req, res) =>{
  res.sendFile(path.join(__dirname, 'html_pages', 'index.html'))
});

app.get ('/menu', (req, res) => {
  res.sendFile(path.join(__dirname, 'html_pages', 'menu.html'))
});


// Posting data from a form with Json response
app.post('/contact', (req, res) => {
  res.json({ msg: "It's done"}); 
})
  
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Serever started on po ${PORT}`));







   

    
